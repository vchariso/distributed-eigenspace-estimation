#!/usr/bin/env julia

using ArgParse
using CSV
using DataFrames
using LinearAlgebra
using Random
using Statistics

include("DisEst.jl")
include("Utils.jl")


"""
    genMat(base_rank, add_rank, δ) -> (K, Σ)

Generates a symmetric matrix with stable rank roughly equal to
`base_rank + add_rank` and eigengap equal to `δ`.
"""
function genMat(base_rank, add_rank, δ, d)
    ρ = 1.0 - ((1 - δ) / add_rank)
    Σ = vcat(fill(1.0, base_rank),
             (1 - δ) .* (ρ.^ (0:(d - base_rank - 1))) .+ 1e-14)  # add 1e-15 to avoid PosDefException
    U = Matrix(qr(randn(d, d)).Q)
    return U * Diagonal(Σ) * U', Diagonal(Σ)
end

function evalConfig(base_rank, add_rs, δ, reps, n_iter)
    n, m, d = 500, 100, 250; nr = length(add_rs)
    eFix = fill(0.0, nr)
    eItr = fill(0.0, nr)
    eAvg = fill(0.0, nr)
    eErm = fill(0.0, nr)
    eRot = fill(0.0, nr)
    eThe = fill(0.0, nr)
    eSrs = fill(0.0, nr)  # stable ranks
    fnTheory(r₀) = begin
        return (r₀ + log(m)) / (δ^2 * n) +
            sqrt((r₀ + 2 * log(n)) / (δ^2 * n * m))
    end
    for (ridx, r) in enumerate(add_rs)
        @info("Trying added rank = $(r)...")
        dsFix = fill(0.0, reps)
        dsItr = fill(0.0, reps)
        dsErm = fill(0.0, reps)
        dsAvg = fill(0.0, reps)
        dsRot = fill(0.0, reps)
        sRank = fill(0.0, reps)
        for rep = 1:reps
            # generate low-rank covariance matrix with given eig-gap
            K, Σ = genMat(base_rank, r, δ, d)
            sRank[rep] = tr(Σ) / maximum(Σ)  # stable rank
            @show sRank[rep]
            dsFix[rep], dsItr[rep], dsErm[rep], dsAvg[rep], dsRot[rep] =
                DisEst.evalIter(K, Σ, n, m, base_rank, n_iter=n_iter)
        end
        # median distances over runs
        eFix[ridx] = median(dsFix)
        eItr[ridx] = median(dsItr)
        eErm[ridx] = median(dsErm)
        eAvg[ridx] = median(dsAvg)
        eRot[ridx] = median(dsRot)
        eThe[ridx] = fnTheory(r)
        # average stable rank
        eSrs[ridx] = mean(sRank)
    end
    # write output to CSV file
    fname = "error-$(base_rank)_maxr-$(maximum(add_rs))_n_iter-$(n_iter).csv"
    CSV.write(fname, DataFrame(srs=eSrs, erm=eErm, itr=eItr, fix=eFix, rot=eRot,
                               avg=eAvg, theo=eThe))
end

s = ArgParseSettings(description="Compare the performance of different " *
                     "estimation methods for distributed low-rank PCA.")
@add_arg_table! s begin
    "--rank_list"
        help     = "A list of ranks to be added to the base stable rank"
        arg_type = Int
        nargs    = '+'
    "--rank"
        help     = "The rank of the desired subspace"
        arg_type = Int
        default  = 5
    "--seed"
        help     = "The seed of the RNG"
        arg_type = Int
        default  = 123
    "--reps"
        help     = "The number of runs averaged to generate a data point"
        arg_type = Int
        default  = 10
    "--delta"
        help     = "The desired eigengap"
        arg_type = Float64
        default  = 0.2
    "--n_iter"
        help     = "The number of iterations for refinement"
        arg_type = Int
        default  = 10
end
parsed = parse_args(s); Random.seed!(parsed["seed"])
rank_list, rank, reps = parsed["rank_list"], parsed["rank"], parsed["reps"]
δ, n_iter = parsed["delta"], parsed["n_iter"]
@show rank_list
evalConfig(rank, rank_list, δ, reps, n_iter)
