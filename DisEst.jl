#!/usr/bin/env julia

module DisEst

    using Arpack
    using Distributions
    using LinearAlgebra
    using Random
    using StatsBase
    using Statistics

    include("Utils.jl")


    """
        rotinvAggregate(Ws, r) -> W

    Compute an averaged leading subspace estimate by aggregating all the local
    subspace projectors and taking a truncated SVD or rank `r`.
    """
    function rotinvAggregate(Ws, r)
        mapped = mapslices(X -> X * X', Ws, dims=(1, 2))
        return svds(mean(mapped, dims=3)[:, :, 1], nsv=r)[1].V
    end



    """
        procrustesFixing(Ws) -> W

    Compute an averaged leading subspace estimate by biasing each of the local
    estimates towards the first using the Procrustes rotation. Assumes that
    `Ws` is an `n x k x m` tensor, where the last dimension indexes the local
    machine.
    """
    function procrustesFixing(Ws)
        Wbase  = Ws[:, :, 1]   # pick first matrix as base matrix
        mapped = mapslices(X -> Utils.procrustesRot(X, Wbase), Ws, dims=(1, 2))
        return Matrix(qr(mean(mapped, dims=3)[:, :, 1]).Q)
    end


    """
        procrustesFixing(Ws, n_iter)

    Compute an averaged estimate by biasing each of the local estimates towards
    the first using the Procrustes rotation. Iteratively refines the estimate
    for `n_iter` iterations.
    """
    function procrustesFixing(Ws, n_iter)
        Wbase  = Ws[:, :, 1]
        mapped = mapslices(X -> Utils.procrustesRot(X, Wbase), Ws, dims=(1, 2))
        Wbase  = Matrix(qr(mean(mapped, dims=3)[:, :, 1]).Q)
        for it = 2:n_iter
            # iterative refine procrustes-fixed iterate
            mapped = mapslices(X -> Utils.procrustesRot(X, Wbase), mapped, dims=(1, 2))
            Wbase  = Matrix(qr(mean(mapped, dims=3)[:, :, 1]).Q)
        end
        return Wbase
    end


    """
        genLowrankCovmat(d, r, δ) -> `K, Σ`

    Generate a low-rank covariance `K` of the multivariate Gaussian ensuring
    that the eigengap is at least `δ`. Also returns the matrix of eigenvalues
    `Σ`.
    """
    function genLowrankCovmat(d, r, δ)
        step = (1 - δ) / (r - 1)
        Σ = vcat(collect(δ * (r:-1:1)), 1e-10 * (1:(d - r)))
        U = Matrix(qr(randn(d, d)).Q)
        return U * Diagonal(Σ) * U', Diagonal(Σ)
    end


    """
        genFullrankCovmat(d, r, δ; low=0.5, high=1.0) -> (K, Σ)

    Generate a full-rank covariance matrix in `d x d` with `r` eigenvalues
    evenly spaced between `high` and `low` (default: [0.5, 1.0]) and `d-r`
    trailing eigenvalues decaying exponentially by a factor of 0.9 starting
    at `low - δ`.

    Returns:
    - `K`: the covariance matrix generated
    - `Σ`: the diagonal matrix containing the eigenvalues of `K`.
    """
    function genFullrankCovmat(d, r, δ; low=0.5, high=1.0)
        Σ = vcat(range(high, stop=low, length=r),
                 (low - δ) .* ((0.9) .^ (0:(d - r - 1))))
        U = Matrix(qr(randn(d, d)).Q)
        return U * Diagonal(Σ) * U', Diagonal(Σ)
    end


    """
        genCovmat(d, r) -> (K, Σ)

    Generate the covariance matrix `K` of the multivariate Gaussian ensuring
    that the eigengap is at least `(1 / (r + 1))`. Also returns the matrix of
    eigenvalues `Σ`.
    """
    function genCovmat(d, r)
        δ = 1 / (r + 1)
        Σ = vcat(1 .- (0:(r-1)) .* δ, (1 - r * δ) .* (0.9).^(1:d - r))
        U = Matrix(qr(randn(d, d)).Q)
        return U * Diagonal(Σ) * U', Diagonal(Σ)
    end



    """
        evalLocalPCA(n, d, m, r; lowrank=false) -> (dFix, dErm, dAvg, dRot)

    Evaluate the local PCA with procrustes fixing for different parameters
    `n, d, m, r` using the synthetic data from Garber et. al. Returns the
    distance of the procrustes-averaged as well as the ERM solution from the
    true eigenvector.

    Returns:
    - `dFix, dErm, dAvg`: the distance to the subspace of the procrustes-fixed,
      centralized ERM, naively averaged and rotation-invariant method solutions
      respectively.
    """
    function evalLocalPCA(n, d, m, r; lowrank=false)
        # K, Σ = lowrank ? genLowrankCovmat(d, r, 0.2) : genCovmat(d, r)
        K, Σ = genFullrankCovmat(d, r, 0.1)
        return evalLocalPCA(K, Σ, n, m, r)
    end


    """
        evalLocalPCA(K, Σ, n, m, r) -> (dFix, dErm, dAvg)

    Evaluate the local PCA with procrustes fixing for a given covariance matrix
    `K` with eigenvalue matrix `Σ` over `m` machines each of which samples `n`
    data points.
    Returns the distance of the procrustes-averaged, ERM, naively averaged, and
    rotation-invariant method solutions from the true eigenvector(s).

    Returns:
    - `dFix, dErm, dAvg`: the distance to the subspace of the procrustes-fixed,
      centralized ERM, naively averaged and rotation-invariant method solutions
      respectively.
    """
    function evalLocalPCA(K, Σ::Diagonal{T, Array{T, 1}} where T <: Real,
                          n, m, r; rnd_seed=123)
        d = size(K)[1]
        # generate all points - reset seed before generating
        Random.seed!(rnd_seed)
        D = MultivariateNormal(Symmetric(K))
        X = permutedims(reshape(rand(D, n * m), d, n, m), [2, 1, 3])
        # generate all covariance matrices and SVDs
        # Xs: local covmats, Ws: local subspaces
        Xs = mapslices(A -> (1 / size(A, 1)) .* (A'A), X, dims=(1, 2))
        Ws = mapslices(A -> svds(A, nsv=r)[1].V, Xs, dims=(1, 2))
        # apply procrustesFixing to local solutions
        Wfix = procrustesFixing(Ws)
        # compare with rotation-invariant averaging
        Wrot = rotinvAggregate(Ws, r)
        # centralized matrix used in ERM
        Xagg = mean(Xs, dims=3)[:, :, 1]
        Werm = svds(Xagg, nsv=r)[1].V
        Wopt = svds(K, nsv=r)[1].V
        Wavg = svd(mean(Ws, dims=3)[:, :, 1]).U
        dFix = opnorm(Wopt - Wfix * (Wfix'Wopt))^2
        dErm = opnorm(Wopt - Werm * (Werm'Wopt))^2
        dAvg = opnorm(Wopt - Wavg * (Wavg'Wopt))^2
        dRot = opnorm(Wopt - Wrot * (Wrot'Wopt))^2
        return dFix, dErm, dAvg, dRot
    end


    function evalIter(K, Σ::Diagonal{T, Array{T, 1}} where T <: Real,
                      n, m, r; rnd_seed=123, n_iter=10)
        d = size(K)[1]
        # generate all points - reset seed before generating
        Random.seed!(rnd_seed)
        D = MultivariateNormal(Symmetric(K))
        X = permutedims(reshape(rand(D, n * m), d, n, m), [2, 1, 3])
        # generate all covariance matrices and SVDs
        # Xs: local covmats, Ws: local subspaces
        Xs = mapslices(A -> (1 / size(A, 1)) .* (A'A), X, dims=(1, 2))
        Ws = mapslices(A -> svds(A, nsv=r)[1].V, Xs, dims=(1, 2))
        # apply procrustesFixing to local solutions
        Wfix = procrustesFixing(Ws)
        Witr = procrustesFixing(Ws, n_iter)
        # compare with rotation-invariant averaging
        Wrot = rotinvAggregate(Ws, r)
        # centralized matrix used in ERM
        Xagg = mean(Xs, dims=3)[:, :, 1]
        Werm = svds(Xagg, nsv=r)[1].V
        Wopt = svds(K, nsv=r)[1].V
        Wavg = svd(mean(Ws, dims=3)[:, :, 1]).U
        dFix = opnorm(Wopt - Wfix * (Wfix'Wopt))^2
        dErm = opnorm(Wopt - Werm * (Werm'Wopt))^2
        dAvg = opnorm(Wopt - Wavg * (Wavg'Wopt))^2
        dRot = opnorm(Wopt - Wrot * (Wrot'Wopt))^2
        dItr = opnorm(Wopt - Witr * (Witr'Wopt))^2
        return dFix, dItr, dErm, dAvg, dRot
    end


    """
        evalQuadSensing(X, m, n; η=0.0) -> (dFix, dErm)

    Evaluate a distributed spectral initialization for phase retrieval, given
    a "tall" matrix `X` and a desired number of machines `m` and `n` samples
    per machine. Optionally, add Gaussian noise of intensity `η`.

    Returns:
    - `dFix, dErm`: the distances of the procrustes-fixed weak estimate and the
      centralized estimate
    """
    function evalQuadSensing(X, m, n; η=0.0, n_iter=1)
        # make orthogonal if not already
        X = Matrix(qr(X).Q)
        N = m * n
        d, r = size(X)
        A = randn(N, d)
        y = sum((A * X).^2, dims=2) .+ (η .* randn(N))
        τ = mean(y)
        # generate all truncated stuff
        T = y .* (y .> (1/2) * τ)
        # multiply with sqrt(T) to make splitting easier
        As = (sqrt.(T)) .* A
        # split into m blocks
        Ds = permutedims(reshape(As', (d, n, m)), [2, 1, 3])
        Xs = mapslices(Z -> (1 / size(Z, 1)) * Z'Z, Ds, dims=(1,2))
        Ws = mapslices(Z -> svds(Z, nsv=r)[1].V, Xs, dims=(1, 2))
        # compare with ERM
        Xagg = mean(Xs, dims=3)[:, :, 1]
        Werm = svds(Xagg, nsv=r)[1].V
        Wfix = procrustesFixing(Ws, n_iter)
        dFix = opnorm(X - Wfix * (Wfix'X))
        dErm = opnorm(X - Werm * (Werm'X))
        return dFix, dErm
    end

end
