#!/usr/bin/env julia

using ArgParse
using CSV
using DataFrames
using LinearAlgebra
using Random
using Statistics

include("DisEst.jl")
include("Utils.jl")

function evalConfig(r, reps, nSamples, mList)
    d = 300
    δ = 0.2
    eFix = fill(0.0, length(mList))
    eAvg = fill(0.0, length(mList))
    eErm = fill(0.0, length(mList))
    eRot = fill(0.0, length(mList))
    eItr = fill(0.0, length(mList))
    for (m_idx, m) in enumerate(mList)
        n = nSamples ÷ m
        @info("Trying m = $(m)...")
        dsFix = fill(0.0, reps)
        dsErm = fill(0.0, reps)
        dsAvg = fill(0.0, reps)
        dsRot = fill(0.0, reps)
        dsItr = fill(0.0, reps)
        for rep = 1:reps
            K, Σ = DisEst.genFullrankCovmat(d, r, δ)
            dsFix[rep], dsItr[rep], dsErm[rep], dsAvg[rep], dsRot[rep] =
                DisEst.evalIter(K, Σ, n, m, r, n_iter=2)
        end
        # mean distances over runs
        eFix[m_idx] = mean(dsFix)
        eErm[m_idx] = mean(dsErm)
        eAvg[m_idx] = mean(dsAvg)
        eRot[m_idx] = mean(dsRot)
        eItr[m_idx] = mean(dsItr)
    end
    # write output to CSV file
    fname = "error_machines-$(r)_reps-$(reps).csv"
    CSV.write(fname, DataFrame(m=mList, erm=eErm, fix=eFix, itr=eItr, rot=eRot, avg=eAvg))
end

s = ArgParseSettings(description="Evaluate the influence of the number of " *
                     "machines for distributed PCA.")
@add_arg_table! s begin
    "--dim_list"
        help        = "The dimensions of the target subspaces"
        arg_type    = Int
        nargs       = '+'
    "--num_samples"
        help        = "The total number of samples to use"
        arg_type    = Int
        default     = 10000
    "--num_machines"
        help        = "The number of machines to use"
        arg_type    = Int
        nargs       = '+'
    "--seed"
        help        = "The seed of the RNG"
        arg_type    = Int
        default     = 999
    "--reps"
        help        = "The number of runs averaged to generate a data points"
        arg_type    = Int
        default     = 10
end
parsed = parse_args(s); Random.seed!(parsed["seed"])
dim_list, reps = parsed["dim_list"], parsed["reps"]
nSamples, mList = parsed["num_samples"], parsed["num_machines"]
for r in dim_list
    @info("Running r = $(r)")
    evalConfig(r, reps, nSamples, mList)
end
