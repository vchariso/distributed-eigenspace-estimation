#!/usr/bin/env julia

using ArgParse
using Arpack
using CSV
using DataFrames
using LinearAlgebra
using MLDatasets
using Printf
using PyPlot
using Random
using Statistics


BLUE="#519cc8"
PURPLE="#908cc0"

include("DisEst.jl")
include("Utils.jl")

function readDigits()
    trainX = Float64.(reshape(MNIST.traintensor(), 28 * 28, :))
    trainY = MNIST.trainlabels()
    return trainX, trainY
end


function main(m, plot_digits)
    A, Y = readDigits()
    μ = mean(A, dims=2)
    A = A .- μ   # center
    A = mapslices(X -> norm(X) < 1e-16 ? X : normalize(X), A, dims=2)  # unit variance
    As  = Utils.splitData(A, m)    # split into matrices
    Vs  = mapslices(Z -> svds((1 / sqrt(size(Z, 2))) * Z, nsv=2)[1].U, As, dims=(1, 2))
    # get V
    Werm = svds((1 / sqrt(size(A, 2))) * A, nsv=2)[1].U
    Wavg = svd(mean(Vs, dims=3)[:, :, 1]).U
    Wfix = DisEst.procrustesFixing(Vs, 1)
    # principal components
    pcErm = A' * Werm
    pcAvg = A' * Wavg
    pcFix = A' * Wfix
    dAvg  = opnorm(Werm - Wavg * (Wavg'Werm))
    dFix  = opnorm(Werm - Wfix * (Wfix'Werm))
    @info("Distances -- (avg, fixed): $(@sprintf("%.3f, %.3f", dAvg, dFix))")
    if plot_digits
        keepErm = pcErm[1:20000, :]
        keepAvg = pcAvg[1:20000, :]
        keepFix = pcFix[1:20000, :]
        rcParams = PyPlot.PyDict(PyPlot.matplotlib."rcParams")
        rcParams["text.usetex"] = true
        rcParams["svg.fonttype"] = "none"
        for dig in [4, 8]
            inds = (Y[1:20000] .== dig)
            fig = figure()
            scatter(keepErm[inds, 1], keepErm[inds, 2], marker="x", color="black", s=8)
            scatter(keepFix[inds, 1], keepFix[inds, 2], facecolors="none", edgecolors=BLUE, s=8)
            scatter(keepAvg[inds, 1], keepAvg[inds, 2], marker="+", color=PURPLE, s=8)
            legend(["Central", "Aligned", "Averaged"], fontsize="x-large", markerscale=2.0)
            savefig("pca_$(dig).pdf")
        end
    end
end


s = ArgParseSettings(description="Run distributed PCA on MNIST.")
@add_arg_table! s begin
    "--num_machines"
        help        = "The number of machines"
        arg_type    = Int
        default     = 50
    "--seed"
        help        = "The seed of the RNG"
        arg_type    = Int
        default     = 999
    "--plot_digits"
        help        = "Set to plot results on digits 4 and 8 to file"
        action      = :store_true
end
parsed = parse_args(s); Random.seed!(parsed["seed"])
m, plot_digits = parsed["num_machines"], parsed["plot_digits"]
main(m, plot_digits)
