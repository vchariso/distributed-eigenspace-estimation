#!/usr/bin/env julia

using ArgParse
using CSV
using DataFrames
using LinearAlgebra
using Printf
using Random
using Statistics

include("DisEst.jl")
include("Utils.jl")


function evalConfig(r, reps, δ, n_iter)
    d, m = 300, 50
    # range of n
    ns = 2 .^ (6:11)
    eFix = fill(0.0, length(ns))
    eItr = fill(0.0, length(ns))
    eCnt = fill(0.0, length(ns))
    eAvg = fill(0.0, length(ns))
    eRot = fill(0.0, length(ns))
    for (n_idx, n) in enumerate(ns)
        @info("Trying n = $(n)")
        dsFix = fill(0.0, reps)
        dsCnt = fill(0.0, reps)
        dsItr = fill(0.0, reps)
        dsAvg = fill(0.0, reps)
        dsRot = fill(0.0, reps)
        for rep = 1:reps
            K, Σ = DisEst.genFullrankCovmat(d, r, δ)
            dsFix[rep], dsItr[rep], dsCnt[rep], dsAvg[rep], dsRot[rep] =
                DisEst.evalIter(K, Σ, n, m, r, n_iter=n_iter)
        end
        # note: evalLocalPCA returns squared dists!
        eFix[n_idx] = sqrt(mean(dsFix))
        eItr[n_idx] = sqrt(mean(dsItr))
        eCnt[n_idx] = sqrt(mean(dsCnt))
        eAvg[n_idx] = sqrt(mean(dsAvg))
        eRot[n_idx] = sqrt(mean(dsRot))
    end
    # write output to CSV file
    fname = "error_niter-$(n_iter)_srank-$(r)-gap-$(@sprintf("%.2f", δ)).csv"
    CSV.write(fname, DataFrame(n=ns, fix=eFix, itr=eItr, erm=eCnt,
                               avg=eAvg, rot=eRot))
end

s = ArgParseSettings(description="Compare the performance of a distributed " *
                     "estimation method for PCA with its iteratively refined " *
                     "version and other estimation methods.")
@add_arg_table! s begin
    "--r"
        help        = "The dimension of the target subspaces"
        arg_type    = Int
        default     = 10
    "--delta"
        help        = "The eigengap size"
        arg_type    = Float64
        default     = 0.1
    "--seed"
        help        = "The seed of the RNG"
        arg_type    = Int
        default     = 999
    "--reps"
        help        = "The number of runs averaged to generate a data points"
        arg_type    = Int
        default     = 5
    "--n_iter"
        help        = "The number of iterations for refinement in Procrustes fixing"
        arg_type    = Int
        default     = 5
end
parsed = parse_args(s); Random.seed!(parsed["seed"])
r, reps, δ, n_iter = parsed["r"], parsed["reps"], parsed["delta"], parsed["n_iter"]
evalConfig(r, reps, δ, n_iter)
