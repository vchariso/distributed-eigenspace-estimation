# distributed-eigenspace-estimation

Code accompanying the paper "Communication-efficient distributed eigenspace estimation" (see [here](https://arxiv.org/abs/2009.02436)).

### Summary
The file `DisEst.jl` contains the implementation of the Procrustes-fixing method, with some auxiliary methods in `Utils.jl`.

The scripts named `test<name>.jl` contain the code used for the numerical experiments in the paper. In particular:

* `testMNIST.jl`: code used to produce Figure 1, comparing naive averaging with principled averaging in the MNIST PCA example.
* `testPCA.jl, testMachines.jl`: implements the synthetic experiments used to produce Figures 2 & 3 in the manuscript.
* `testIterative.jl`: compares the performance of Algorithms 1 & 2, as well as the algorithm of ref. \[19\], with the central algorithm.
* `testStableRank.jl, testTheory.jl`: experiments on the effect of stable rank and consistence between theoretical and empirical error rate for synthetic instances.
* `testSensing.jl`: experiments on distributed spectral initialization for quadratic sensing.

All scripts use the `ArgParse.jl` package to provide command-line options and documentation. To get help about how to run a script, run

```bash
julia <scriptname> --help
```

Because most of the figures in the manuscript are generated using TikZ/PGFPlots, almost all scripts (with the exception of `testMNIST.jl`)
output a `.csv` file containing the data used to generate the figures in LaTeX. To access the source code used to generate them, you may
download the `.tex` source from the [here](https://arxiv.org/format/2009.02436) and look under `chapters/numerics.tex`, which includes
the PGFPlots source.

### Dependencies
The core utilities depend on `Arpack.jl`, `Distributions.jl`, and `StatsBase.jl`. All script files require `Argparse.jl`, `CSV.jl`, `DataFrames.jl`;
`testMNIST.jl` additionally requires `MLDatasets` and `PyPlot`.
