#!/usr/bin/env julia

using ArgParse
using CSV
using DataFrames
using LinearAlgebra
using Printf
using Random
using Statistics

include("DisEst.jl")
include("Utils.jl")


function evalConfig(r, reps, δ)
    d, m = 300, 100
    p, c = 1.0, 1
    # theory rate using stable rank
    fnTheory(r₀, n) = begin
        return (r₀ + log(m / p)) / (δ^2 * n) +
            sqrt((r₀ + 2 * log(n)) / (δ^2 * n * m))
    end
    # range of n
    ns = 2 .^ (7:14)
    eFix = fill(0.0, length(ns))
    eThe = fill(0.0, length(ns))
    eCnt = fill(0.0, length(ns))
    for (n_idx, n) in enumerate(ns)
        @info("Trying n = $(n)")
        dsFix = fill(0.0, reps)
        dsCnt = fill(0.0, reps)
        for rep = 1:reps
            K, Σ = DisEst.genFullrankCovmat(d, r, δ)
            rstb = tr(Σ) / opnorm(Σ)  # stable rank
            # set rate - each stable rank will be the same across reps
            eThe[n_idx] = min(1.0, fnTheory(rstb, n))
            dsFix[rep], dsCnt[rep], _, _ =
                DisEst.evalLocalPCA(K, Σ, n, m, r)
        end
        # median distances over runs - evalLocalPCA returns squared dists!
        eFix[n_idx] = sqrt(median(dsFix))
        eCnt[n_idx] = sqrt(median(dsCnt))
    end
    # write output to CSV file
    fname = "error_theory-$(r)_gap-$(@sprintf("%.2f", δ)).csv"
    CSV.write(fname, DataFrame(n=ns, fix=eFix, erm=eCnt, theo=eThe))
end

s = ArgParseSettings(description="Compare the performance of a distributed " *
                     "estimation method for PCA with its theoretical rate.")
@add_arg_table! s begin
    "--r"
        help        = "The dimension of the target subspaces"
        arg_type    = Int
        default     = 10
    "--delta"
        help        = "The eigengap size"
        arg_type    = Float64
        default     = 0.2
    "--seed"
        help        = "The seed of the RNG"
        arg_type    = Int
        default     = 999
    "--reps"
        help        = "The number of runs averaged to generate a data points"
        arg_type    = Int
        default     = 15
end
parsed = parse_args(s); Random.seed!(parsed["seed"])
r, reps, δ = parsed["r"], parsed["reps"], parsed["delta"]
evalConfig(r, reps, δ)
