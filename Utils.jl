module Utils

    using Arpack
    using LinearAlgebra
    using Random


    # max singular value
    σMax(A) = first(svds(A, nsv=1, ritzvec=false)[1].S)


    """
        lowRankCovmat(d, r, rank, δ; low=0.5, high=1.0) -> (K, Σ)

    Generate a low-rank covariance matrix in `d x d` with `rank` eigenvalues
    evenly spaced between `high` and `low` (default: [0.5, 1.0]) and `r - rank`
    trailing eigenvalues decaying exponentially by a factor of 0.9 starting
    at `low - δ`. The rest of the eigenvalues are set to `0`.

    Returns:
    - `K`: the covariance matrix generated
    - `Σ`: the diagonal matrix containing the eigenvalues of `K`.
    """
    function lowRankCovmat(d, r, rank, δ; low=0.5, high=1.0)
        U = Matrix(qr(randn(d, d)).Q)
        Σ = vcat(range(high, stop=low, length=rank),
                 (low - δ) .* ((0.9) .^ (0:(d - rank - 1))))
        Σ[(r+1):end] .= 1e-15
        return U * Diagonal(Σ) * U', Diagonal(Σ)
    end


    """
        genImbalancedMatrix(m, n)

    Generate a matrix with imbalanced leverage scores across its row submatrices.
    """
    function genImbalancedMatrix(n, nBlock, bSize)
        m = nBlock * bSize; A = fill(0.0, (m, n))
        σs = sqrt.(2.0.^(-(0:(nBlock - 1))))
        @inbounds for b = 1:nBlock
            inds = ((b - 1) * bSize + 1):(b * bSize)
            Σ = Diagonal(σs[b].^(-((0:(bSize - 1)) ./ (bSize - 1))))
            U = Matrix(qr(randn(bSize, bSize)).Q)
            V = Matrix(qr(randn(n, bSize)).Q)
            A[inds, :] = U * Σ * V'
        end
        return A
    end


    """
        proj_simplex(y)

    Project a vector `y` to the unit simplex using the `O(d logd)` algorithm by
    Duchi et. al.
    """
    function proj_simplex(y)
        u = sort(y, rev=true)
        c = cumsum(u)
        r = findlast(u + ((1 .- c) ./ (1:length(u))) .> 0)
        l = (1 / r) * (1 - sum(u[1:r]))
        return max.(y .+ l, 0)
    end


    """
        procrustesRot(W, Wbase) -> W

    Rotate an orthogonal matrix `W` so that it optimally aligns with `Wbase`
    in the Frobenius norm, returning the rotated matrix.
    """
    function procrustesRot(W, Wbase)
        S = svd(W'Wbase)
        return W * (S.U * S.Vt)
    end


    """
        procrustesDist(X, Y) -> dist

    Compute the Procrustes distance between matrices `X` and `Y`.
    """
    function procrustesDist(X, Y)
        Wrot = procrustesRot(X, Y)
        return norm(Y - X)
    end


    """
        splitData(A, m) -> Vs

    Split the column vectors in `A` into `m` machines.
    """
    function splitData(A, m)
        d, n = size(A)
        perm = randperm(n)
        nPer = n ÷ m
        Vs   = fill(0.0, (d, nPer, m))
        for m_idx=1:m
            iStart = (m_idx - 1) * nPer + 1
            iEnd   = m_idx * nPer
            Vs[:, :, m_idx] = A[:, perm[iStart:iEnd]]
        end
        return Vs
    end


    """
        optimal_average(Xs, xopt; Tmax=25)

    Compute the optimal averaging coefficients by solving a constrained least
    squares program.
    """
    function optimal_average(Xs, xopt; Tmax=25)
        h = rand(size(Xs, 2)); h = h ./ sum(h)
        λBase = 0.5 / (σMax(Xs)^2); λ = λBase
        @debug("λBase: $(λBase)")
        for t = 1:Tmax
            h = proj_simplex(h - λ * (Xs' * (Xs * h - xopt)))
        end
        return h
    end


    function blockSketch(A, bSize, s, ϵ)
        m, n = size(A)
        nblocks = m ÷ bSize
        (m % bSize != 0) && throw(ErrorException("m ($(m)) must be an exact " *
                                                 "multiple of the block size" *
                                                 "$(bSize)"))
        # theoretical size - can't go over bSize
        s = min(n * log(n) * log(m) / (nblocks * ϵ^2), bSize)
        # TODO: Finish
    end

end

