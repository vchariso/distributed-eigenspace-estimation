#!/usr/bin/env julia

using ArgParse
using CSV
using DataFrames
using LinearAlgebra
using Random
using Statistics

include("DisEst.jl")
include("Utils.jl")

function evalConfig(r, reps, lowrank)
    mulM = 25
    mulN = 25
    numM = 2
    numN = 20
    d = 300
    eFix = fill(0.0, (numN, numM))
    eAvg = fill(0.0, (numN, numM))
    eErm = fill(0.0, (numN, numM))
    eRot = fill(0.0, (numN, numM))
    for m_idx in 1:numM
        m = m_idx * mulM
        @info("Trying m = $(m)...")
        for n_idx = 1:numN
            n = n_idx * mulN
            @info("Trying n = $(n_idx * mulN)")
            dsFix = fill(0.0, reps)
            dsErm = fill(0.0, reps)
            dsAvg = fill(0.0, reps)
            dsRot = fill(0.0, reps)
            for rep = 1:reps
                dsFix[rep], dsErm[rep], dsAvg[rep], dsRot[rep] =
                    DisEst.evalLocalPCA(n, d, m, r, lowrank=lowrank)
            end
            # mean distances over runs
            eFix[n_idx, m_idx] = mean(dsFix)
            eErm[n_idx, m_idx] = mean(dsErm)
            eAvg[n_idx, m_idx] = mean(dsAvg)
            eRot[n_idx, m_idx] = mean(dsRot)
        end
    end
    # write output to CSV file
    fname = "error_normal-$(r)_reps-$(reps)_lowrank-$(lowrank).csv"
    CSV.write(fname, DataFrame(n=(1:numN) .* 25, erm25=eErm[:, 1],
                               fix25=eFix[:, 1], avg25=eAvg[:, 1],
                               rot25=eRot[:, 1], erm50=eErm[:, 2],
                               fix50=eFix[:, 2], avg50=eAvg[:, 2],
                               rot50=eRot[:, 2]))
end

s = ArgParseSettings(description="Compare the performance of different " *
                     "estimation methods for distributed PCA.")
@add_arg_table! s begin
    "--dim_list"
        help        = "The dimensions of the target subspaces"
        arg_type    = Int
        nargs       = '+'
    "--seed"
        help        = "The seed of the RNG"
        arg_type    = Int
        default     = 999
    "--reps"
        help        = "The number of runs averaged to generate a data points"
        arg_type    = Int
        default     = 10
    "--lowrank"
        help        = "Set to use a low-rank covariance matrix"
        action      = :store_true
end
parsed = parse_args(s); Random.seed!(parsed["seed"])
dim_list, reps, lowrank = parsed["dim_list"], parsed["reps"], parsed["lowrank"]
@show dim_list
for r in dim_list
    @info("Running r = $(r)")
    evalConfig(r, reps, lowrank)
end
