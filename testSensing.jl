#!/usr/bin/env julia

using ArgParse
using CSV
using DataFrames
using LinearAlgebra
using Printf
using Random
using Statistics

include("DisEst.jl")
include("Utils.jl")


function evalConfig(d, r, reps, n_iter)
    m = 30
    n = r * trunc(Int, d)
    iList = [1, 2, 3, 4, 5, 6, 7, 8]
    X = randn(d, r)
    eFix = fill(0.0, length(iList))
    eErm = fill(0.0, length(iList))
    for i in iList
        @info("Running i = $(i)")
        dFix = fill(0.0, reps)
        dErm = fill(0.0, reps)
        for r in reps
            dFix, dErm = DisEst.evalQuadSensing(X, m, i * n, n_iter=n_iter)
        end
        eFix[i] = median(dFix)
        eErm[i] = median(dErm)
    end
    # write output to CSV file
    fname = "error_quadsensing_$(d)x$(r)_n_iter-$(n_iter).csv"
    CSV.write(fname, DataFrame(i=iList, fix=eFix, erm=eErm))
end

s = ArgParseSettings(description="Compare the performance of a distributed " *
                     "estimation method for PCA with its theoretical rate.")
@add_arg_table! s begin
    "--d"
        help        = "The row dimension of the latent matrix signal"
        arg_type    = Int
        default     = 100
    "--r"
        help        = "The column dimension of the latent matrix signal"
        arg_type    = Int
        default     = 5
    "--seed"
        help        = "The seed of the RNG"
        arg_type    = Int
        default     = 999
    "--reps"
        help        = "The number of runs averaged to generate a data point"
        arg_type    = Int
        default     = 10
    "--n_iter"
        help        = "Number of iterations for iterative refinement"
        arg_type    = Int
        default     = 1
end
parsed = parse_args(s); Random.seed!(parsed["seed"])
d, r, reps, n_iter = parsed["d"], parsed["r"], parsed["reps"], parsed["n_iter"]
evalConfig(d, r, reps, n_iter)
